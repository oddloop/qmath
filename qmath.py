"""
qmath.py: A Pure Python Quaternion Math Module.
version 0.0.1

Copyright (C) 2018 Alexander Gosselin

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>
"""
#import array as _array
import copy as _copy
import cmath as _cmath
import math as _math
import re as _re

from array import array as _array
from math import e, pi, tau
from math import exp as _exp, log as _log, cos as _cos, sin as _sin, \
    acos as _acos, atan2 as _atan2
from cmath import exp as _cexp, log as _clog, sqrt as _csqrt

class Quaternion:
    """ Quaternion(a, i, j, k)
    """
    __slots__ = ('_array',)

    def __init__(self, a=None, i=None, j=None, k=None):
        try: # default constructor, all arguments given
            self._array = _array("d", (a, i, j, k))
            return
        except TypeError:
            try: # default constructor, some arguments given
                self._array = _array("d", (
                    0.0 if _ is None else _ for _ in (a, i, j, k)))
                return
            except TypeError:
                pass
        if j is None and k is None:
            if i is None:
                try: # copy constructor
                    self._array = a._array.__copy__()
                    return
                except AttributeError:
                    pass
                try: # complex number constructor
                    self._array = _array("d", (a.real, a.imag, 0.0, 0.0))
                    return
                except AttributeError:
                    pass
                try:  # string constructor
                    self._fromstring(a)
                    return
                except TypeError as e:
                    raise TypeError(e)
            elif isinstance(a, complex) and isinstance(i, complex):
                # Cayley-Dickson construction from two complex numbers
                self._array = _array("d", (a.real, a.imag, i.real, i.imag))
                return
        raise TypeError(( # no constructor succeeded
            "%s() can take up to four real arguments, two complex numbers, or "
            "one quaternion or one string. %i arguments of type %s provided."
        ) % (self.__class__.__name__,
             len([_ for _ in (a, i, j, k) if _ is not None ]),
             tuple(type(_).__name__ for _ in (a, i, j, k) if _ is not None)))
    
    def _fromstring(self, string):
        if string[0] is '(' and string[-1] is ')': # remove parens
            string = string[1:-1]
        leading_a = _re.compile( # matches if leading term is real
            "([-+]?(?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))"
            "(?:([-+](?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))i)?"
            "(?:([-+](?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))j)?"
            "(?:([-+](?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))k)?")
        leading_i = _re.compile( # matches if leading term is i component
            "()"
            "(?:([-+]?(?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))i)"
            "(?:([-+](?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))j)?"
            "(?:([-+](?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))k)?")
        leading_j = _re.compile( # matches if leading term is j component
            "()()"
            "(?:([-+]?(?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))j)"
            "(?:([-+](?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))k)?")
        leading_k = _re.compile( # matches if leading term is k component
            "()()()"
            "(?:([-+]?(?:(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?|inf|nan))k)")
        for regex in (leading_a, leading_i, leading_j, leading_k):
            try:          
                self._array = _array("d", (float(_) if _ else 0.0 for _ in
                                           regex.fullmatch(string).groups()))
                return
            except AttributeError:
                pass
        raise ValueError("%s() argument '%s' is a malformed string" \
                         % (type(self).__name__, string))
    
    # @property
    # def a(self):
    #     """ Return the real part of the quaternion. Equivalent to self.real. """
    #     return self._array[0]
    # @a.setter
    # def a(self, value):
    #     self._array[0] = value
    
    # @property
    # def i(self):
    #     """ Return the first imaginary component (i) of the quaternion. """
    #     return self._array[1]
    # @i.setter
    # def i(self, value):
    #     self._array[1] = value
    
    # @property
    # def j(self):
    #     """ Return the second imaginary component (j) of the quaternion. """
    #     return self._array[2]
    # @j.setter
    # def j(self, value):
    #     self._array[2] = value
    
    # @property
    # def k(self):
    #     """ Return the third imaginary component (k) of the quaternion. """
    #     return self._array[3]
    # @k.setter
    # def k(self, value):
    #     self._array[3] = value
    
    @property
    def real(self):
        """ Return the real part of the quaternion. """
        return self._array[0]
    
    @property
    def imag(self):
        """ Return the imaginary components of the quaternion as a 3-tuple. """
        return (*self._array[1:],)
        # """ Return the magnitude of the imaginary part of the quaternion.
        # This behaviour is consistent with complex.imag, which returns a float.
        # To get the vector part of the quaternion, use the `vect` property.
        # """
        # return sum(_*_ for _ in self._array[1:])**.5
    
    # @property
    # def vect(self):
    #     """ Return the vector part of the quaternion. """
    #     return (*(self._array[1:]),)
    
    def __abs__(self):
        """ Return the magnitude of the quaternion. Equivalent to:
            sqrt(q[0]**2 + q[1]**2 + q[2]**2 + q[3]**2)
        """
        return sum(_*_ for _ in self._array)**.5
    
    def __bool__(self):
        """ Return True if the quaternion is nonzero. """
        return any(self._array)
    
    def __pos__(self):
        """ Unary plus operator. Equivalent to `copy.copy(self)`. """
        return self.__copy__()
    
    def __neg__(self):
        """ Unary negation operator. """
        result = object.__new__(self.__class__)
        result._array = _array("d", (-_ for _ in self._array))
        return result
    
    def __add__(self, value):
        """ Return self+value. """
        try:
            result = object.__new__(self.__class__)
            result._array = self._array.__copy__()
            return result.__iadd__(value)
        except TypeError:
            raise TypeError(
                "'+' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __radd__(self, value):
        """ Return value+self. """
        try:
            return self.__add__(value) # quaternion addition is commutative
        except TypeError:
            raise TypeError(
                "'+' not supported between instances of '%s' and '%s'" \
                % (type(value).__name__, type(self).__name__))
    
    def __iadd__(self, value):
        try:
            try:
                for _ in range(4):
                    self._array[_] += value._array[_]
            except AttributeError:
                self._array[0] += value.real
                self._array[1] += value.imag
            return self
        except AttributeError:
            raise TypeError(
                "'+=' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __sub__(self, value):
        """ Return self-value. """
        try:
            result = object.__new__(self.__class__)
            result._array = self._array.__copy__()
            result.__isub__(value)
            return result
        except TypeError:
            raise TypeError(
                "'-' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __rsub__(self, value):
        """ Return value-self. """
        try:
            result = self.__neg__()
            result._array[0] += value.real
            result._array[1] += value.imag
            return result
        except AttributeError:
            raise TypeError(
                "'-' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __isub__(self, value):
        try:
            try:
                for _ in range(4):
                    self._array[_] -= value._array[_]
            except AttributeError:
                self._array[0] -= value.real
                self._array[1] -= value.imag
            return self
        except AttributeError:
            raise TypeError(
                "'-=' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __mul__(self, value):
        """ Return self*value. """
        try:
            result = object.__new__(self.__class__)
            try:
                a, b, c, d = self._array
                w, x, y, z = value._array
                result._array = _array("d", (
                    a*w - b*x - c*y - d*z,
                    a*x + b*w + c*z - d*y,
                    a*y - b*z + c*w + d*x,
                    a*z + b*y - c*x + d*w))
                return result
            except AttributeError:
                x = value.real
                y = value.imag
                if y: # value is a complex number
                    a, b, c, d = self._array
                    result._array = _array("d", (
                        a*x - b*y, b*x + a*y, c*x + d*y, d*x - c*y))
                else: # value is a real number
                    result._array = _array("d", (_*x for _ in self._array))
                return result
        except TypeError:
            raise TypeError(
                "'*' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __rmul__(self, value):
        """ Return value*self. """
        try:
            x = value.real
            y = value.imag
            result = object.__new__(self.__class__)
            if y: # value is a complex number
                a, b, c, d = self._array
                result._array = _array("d", (
                    a*x - b*y, b*x + a*y, c*x - d*y, d*x + c*y))
            else: # value is a real number
                result._array = _array("d", (x*_ for _ in self._array))
            return result
        except AttributeError as e:
            raise TypeError(
                "'*' not supported between instances of '%s' and '%s'" \
                % (type(value).__name__, type(self).__name__))
    
    def __imul__(self, value):
        try:
            a, b, c, d = self._array
            w, x, y, z = value._array
            self._array = _array("d", (
                a*w - b*x - c*y - d*z,
                a*x + b*w + c*z - d*y,
                a*y - b*z + c*w + d*x,
                a*z + b*y - c*x + d*w))
            return self
        except AttributeError:
            try:
                x = value.real
                y = value.imag
                if y: # value is a complex number
                    a, b, c, d = self._array
                    self._array = _array("d", (
                        a*x - b*y, b*x + a*y, c*x + d*y, d*x - c*y))
                else: # value is a real number
                    for _ in range(4):
                        self._array[_] *= x
                return self
            except AttributeError:
                raise TypeError(
                    "'*=' not supported between instances of '%s' and '%s'" \
                    % (type(self).__name__, type(value).__name__))
    
    def __truediv__(self, value):
        """ Return self/value. """
        try:
            try:
                return self.__mul__(value.inverse())
            except AttributeError:
                x = value.real
                y = value.imag
                result = object.__new__(self.__class__)
                if y:
                    result._array = self._array.__copy__()
                    return result.__imul__(1/value)
                else:
                    result._array = _array("d", (_/x for _ in self._array))
                    return result
        except TypeError:
            raise TypeError(
                "'/' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __rtruediv__(self, value):
        """ Return value/self. """
        return self.inverse().__rmul__(value)
        raise TypeError(
            "'/' not supported between instances of '%s' and '%s'" \
            % (type(value).__name__, type(self).__name__))
    
    def __itruediv__(self, value):
        """ Return self/=value. """
        try:
            try:
                return self.__imul__(value.inverse())
            except AttributeError:
                x = value.real
                y = value.imag
                if y:
                    return self.__imul__(1/value)
                else:
                    self._array = _array("d", (_/x for _ in self._array))
                    return self
        except TypeError:
            raise TypeError(
                "'/' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __floordiv__(self, _):
        """ '//' is intentionally not implemented. Raise a TypeError. """
        raise TypeError("can't take the floor of a %s." % type(self).__name__)

    def __rfloordiv__(self, _):
        """ '//' is intentionally not implemented. Raise a TypeError. """
        raise TypeError("can't take the floor of a %s." % type(self).__name__)
    
    def __divmod__(self, _):
        """ divmod is intentionally not implemented. Raise a TypeError. """
        raise TypeError("can't take the floor or mod of a %s." \
                        % type(self).__name__)
    
    def __rdivmod__(self, _):
        """ divmod is intentionally not implemented. Raise a TypeError. """
        raise TypeError("can't take the floor or mod of a %s." \
                        % type(self).__name__)
    
    def __pow__(self, value):
        """ Return self**value. """
        try:
            x = value.real
            y = value.imag
            if y or x % 1 != 0:
                return exp(value*log(self))
            x = abs(int(x))
            if x == 0:
                result = object.__new__(self.__class__)
                result._array = _array("d", (1.0, 0.0, 0.0, 0.0))
                return result
            result = self.__copy__()
            for _ in bin(x)[3:]:
                result *= result
                if _ is '1':
                    result *= self
            if value.real < 0:
                return result.inverse()
            return result
        except AttributeError:
            raise TypeError(
                "'**' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __rpow__(self, value):
        """ Return value**self. """
        try:
            return exp(self*_log(value))
        except TypeError:
            raise TypeError(
                "'**' not supported between instances of '%s' and '%s'" \
                % (type(value).__name__, type(self).__name__))
    
    def __eq__(self, value):
        """ Return self==value. """
        try:
            return self._array == value._array
        except AttributeError:
            try:
                a, b, c, d = self._array
                if not (c or d):
                    return a == value.real and b == value.imag
                else:
                    return False
            except AttributeError:
                raise TypeError(
                    "'==' not supported between instances of '%s' and '%s'" \
                    % (type(self).__name__, type(value).__name__))
    
    def __ne__(self, value):
        """ Return self!=value. """
        try:
            return not self == value
        except TypeError:
            raise TypeError(
                "'!=' not supported between instances of '%s' and '%s'" \
                % (type(self).__name__, type(value).__name__))
    
    def __getitem__(self, item):
        """ Getitem is provided for user convenience. """
        try:
            return (*self._array[item],)
        except TypeError:
            return self._array[item]
    
    def __iter__(self):
        """ Implements iter(self). """
        return iter(self._array)

    def __copy__(self):
        copy = object.__new__(self.__class__)
        copy._array = self._array.__copy__()
        return copy

    def __deepcopy__(self):
        return self.__copy__()
    
    def __repr__(self):
        return "Quaternion({:-g}, {:-g}, {:-g}, {:-g})".format(*self._array)

    def __str__(self):
        return self.__format__('g')

    def __format__(self, format_spec):
        string = "({:%s}{:+%s}i{:+%s}j{:+%s}k)" % ((format_spec,)*4)
        return string.format(*self._array)

    def __hash__(self):
        return hash(self._array)
    
    def __complex__(self):
        raise TypeError("can't convert %s to complex" % type(self).__name__)

    def __int__(self):
        raise TypeError("can't convert %s to int" % type(self).__name__)
    
    def __float__(self):
        raise TypeError("can't convert %s to float" % type(self).__name__)

    def conjugate(self):
        result = object.__new__(self.__class__)
        result._array = _array("d", (
            self._array[0], *(-_ for _ in self._array[1:])))
        return result

    def inverse(self):
        s = 1/sum(_*_ for _ in self._array)
        result = object.__new__(self.__class__)
        result._array = _array("d", (
            self._array[0]*s, *(-_*s for _ in self._array[1:])))
        return result

    @classmethod
    def from_euler_angles(cls, α, β, γ, sequence="zyz"):
        raise NotImplementedError

    @classmethod
    def from_tait_bryan_angles(cls, φ, θ, ψ, sequence="zyz"):
        raise NotImplementedError

    @classmethod
    def from_axis_angle(cls, vi, vj, vk, theta):
        t = theta/2
        b = _sin(t)
        return cls(_cos(t), b*vi, b*vj, b*vk)

def exp(q):
    """ Return the quaternionic exponential of q.
    The quaternionic exponential is defined by the formula:
        exp(q) = exp(a)*(cos(|v|) + v*sin(|v|)/|v|)
    where a is the real part of q, v is the vector (imaginary) part of q, and
    the vertical bars denote the euclidean norm of a quantity.
    When a real quaternion (v = 0) is passed as an argument, this function
    returns the logarithm of the real number as a real quaternion.
    Reference:
        <en.wikipedia.org/?curid=51440#Functions_of_a_quaternion_variable>
    """
   try:
        c = _exp(q._array[0])
        v = sum(_*_ for _ in q._array[1:])**.5
        if not v: # q is a real Quaternion.
            result = object.__new__(Quaternion)
            result._array = _array("d", (c, 0.0, 0.0, 0.0))
            return result
        a = c*_cos(v)
        b = c*_sin(v)/v
        result = object.__new__(Quaternion)
        result._array = _array("d", (a, *(b*_ for _ in q._array[1:])))
        return result
    except AttributeError: # q might be complex, float or int.
        return Quaternion(_cexp(q))

def log(q):
    """ Return the quaternionic logarithm of q.
    The quaternionic logarithm is defined by the formula:
        log(q) = log(|q|) + v*arccos(a/|q|)/|v|
    where a is the real part of q, v is the vector (imaginary) part of q, and
    the vertical bars denote the euclidean norm of a quantity. When a real
    quaternion (v = 0) is passed as an argument, this function returns the
    logarithm of the real number as a real quaternion.
    Reference:
        <en.wikipedia.org/?curid=51440#Funct realions_of_a_quaternion_variable>
    """
    try:
        v = sum(_*_ for _ in q._array[1:])**.5
        if not v: # q is a (positive or negative) real Quaternion
            return Quaternion(_clog(q._array[0]))
        r = q.__abs__()
        b = _acos(q._array[0]/r)/v
        result = object.__new__(Quaternion)
        result._array = _array("d", (_log(r), *(b*_ for _ in q._array[1:])))
        return result
    except AttributeError: # q might be complex, float or int.
        return Quaternion(_clog(q))

def sqrt(q):
    """ Return one square root of q.
    The behaviour of this function is intended to be consistent with cmath.sqrt
    which returns `exp(0.5*log(z))` but not `exp(0.5*(log(z + 2*pi)))`. This is
    not a problem because the second root can be obtained by negating the first.
    There are infinitely many quaternion square roots of the negative reals;
    they form pure-imaginary spherical shells with radius sqrt(-x). Of these,
    this function only returns the one on the positive i axis.
    Reference:
       <https://math.stackexchange.com/a/382440/249757>
    """
    try:
        if not any(q._array[1:]): # q is a (positive or negative) real Quaternion
            return Quaternion(_csqrt(q._array[0]))
        return exp(0.5*log(q))
    except AttributeError: # q might be complex, float or int.
        return Quaternion(_csqrt(q))

def polar(q):
    """ Return a polar representation of q.
    This function returns (r, φ, θ, ψ) such that
        q == r*exp(quaternion(0, φ, θ, ψ))
    evaluates to True.
    """
    try:
        r = q.__abs__()
        return (r, *log(q.__truediv__(r))._array[1:])
    except AttributeError:
        return _cmath.polar(q)

def phase(q):
    """ Return the imaginary components of the polar representation of q. """
    try:
        return polar(q)[1:]
    except AttributeError:
        return _cmath.phase(q)
    
def rect(r, phi, theta, psi):
    """ Convert from polar representation to rectangular coordinates. """
    v = (phi*phi + theta*theta + psi*psi)**.5
    if not v:
        result = object.__new__(Quaternion)
        result._array = _array("d", (r, 0.0, 0.0, 0.0))
        return result
    b = r*_sin(v)/v
    result._array = _array("d", (r*_cos(v), b*phi, b*theta, b*psi))
    return result

def rotation_matrix(q):
    """ Return a quaternion-derived rotation matrix.
    Reference:
       <en.wikipedia.org/?curid=186057#Quaternion-derived_rotation_matrix>
    """
    a, b, c, d = q._array
    s2 = 2.0/sum(_*_ for _ in q._array)
    return ((1 - s2*(c*c + d*d), s2*(b*c - a*d), s2*(b*d + a*c)),
            (s2*(b*c + a*d), 1 - s2*(b*b + d*d), s2*(c*d - a*b)),
            (s2*(b*d - a*c), s2*(c*d + a*b), 1 - s2*(b*b + c*c)))

def axis_angle(q):
    """ Return the axis-angle representation of quaternion q as a rotation.
    Usage:
       uᵢ, uⱼ, uₖ, θ = axis_angle(q)
    Reference:
       <en.wikipedia.org/?curid=186057#Recovering_the_axis-angle_representation>
    """
    v = sum(_*_ for _ in q._array[1:])**.5
    return (*(_/v for _ in q._array[1:]),  # axis of rotation
            2*_atan2(v, q._array[0])) # angle of rotation

def euler_angles(q, sequence="zyz"):
    raise NotImplementedError

def tait_bryan_angles(q, sequence="zyz"):
    raise NotImplementedError

def lerp(q0, q1, t):
    """ Linear interpolation between quaternions. """
    s = 1.0 - t
    result = object.__new__(Quaternion)
    result._array = _array("d", (
        a*s + b*t for a, b in zip(q0._array, q1._array)))
    return result

def nlerp(q0, q1, t):
    """ Normalized linear interpolation. """
    result = lerp(q0, q1, t)
    result /= abs(result)
    return result

def slerp(q0, q1, t):
    """ Spherical linear interpolation.
    Reference:
        <en.wikipedia.org/?curid=960929#Quaternion_Slerp>
    """
    try:
        return q0.__mul__(q0.inverse().__mul__(q1).__pow__(t))
    except AttributeError:
        raise TypeError("slerp(q0, q1, t) is only defined for a pair of "
                        "quaternions q0, q1 and a real number t.")

def norm(q, p=None):
    """ Return the Euclidean  or p-norm of the quaternion.
    When p is None, return the Euclidean norm,  equivalent to:
        abs(q) or sqrt(q*q.conjugate())
    When p is not None, this is equivalent to:
        (abs(q.a)**p + abs(q.i)**p + abs(q.j)**p + abs(q.k)**p)**(1/p)
    There are special cases for p = 0, p = 1 and p = ∞.
    """
    if p is None:       # Euclidean norm
        return q.__abs__()
    if p == 0:          # counting norm
        return sum(1 if _ else 0 for _ in q._array)
    if p == 1:          # Manhattan norm
        return sum(abs(_) for _ in q._array)
    if isinf(p): # maximum norm
        return max(abs(_) for _ in q._array)
    return sum(abs(_)**p for _ in q._array)**(1/p)

def isclose(q0, q1, rel_tol=1e-9, abs_tol=0.0):
    """ Determine whether two quaternions are close in value. """
    try:
        return all(_math.isclose(a, b, rel_tol, abs_tol)
                   for a, b in zip(q0._array, q1._array))
    except AttributeError:
        return isclose(Quaternion(q0), Quaternion(q1), rel_tol, abs_tol)

inf  = Quaternion(_math.inf, 0.0, 0.0, 0.0)
infi = Quaternion(0.0, _math.inf, 0.0, 0.0)
infj = Quaternion(0.0, 0.0, _math.inf, 0.0)
infk = Quaternion(0.0, 0.0, 0.0, _math.inf)

def isinf(q):
    try:
        return any(_math.isinf(_) for _ in q._array)
    except AttributeError:
        return _cmath.isinf(q)

def isfinite(q):
    return not isinf(q)

nan  = Quaternion(_math.nan, 0.0, 0.0, 0.0)
nani = Quaternion(0.0, _math.nan, 0.0, 0.0)
nanj = Quaternion(0.0, 0.0, _math.nan, 0.0)
nank = Quaternion(0.0, 0.0, 0.0, _math.nan)

def isnan(q):
    try:
        return any(_math.isnan(_) for _ in q._array)
    except AttributeError:
        return _cmath.isnan(q)

